import datetime
import time
import os
import csv
import spur
import sys
import Queue
import threading

# Set the file names here
credentials = "creds.csv"
dictionary = "passwords.csv"
successlogfile = "credsm.csv"
failurelogfile = "credsf.csv"

# Define various file descriptors and global variables here to be accessed in the functions later
firstrun = False
# How many passwords to skip from the file
skip = 0
# List to store the passwords from the file
passlist = []
# Queue to store the credantials info for use with threaded functions
credsq = Queue.Queue()
# Thread lock to print to stdout and write to files by one thread only at a time
lock = threading.Lock()

# File descriptor for the credentials
credsfile = open(credentials, 'r')
credsfilereader = csv.reader(credsfile)
credslist = list(credsfilereader)

# Get rid of the header on the first line
credslist = credslist[1:]
# Store credentials in the queue for later use
for l in credslist:
    credsq.put(l)
# Successfully executed commands log file
# success = open(successlogfile, "w")
# swriter = csv.writer(success, lineterminator="\n")
# swriter.writerow(("IP", "Username", "Password", "Last Password Change", "Password Expires", "Log", "Fullstamp"))
# Failed execution commands log file
# failure = open(failurelogfile, "w")
# fwriter = csv.writer(failure, lineterminator="\n")
# fwriter.writerow(("IP", "Username", "Password", "Log", "Fullstamp"))
# Dictionary fd
f = open(dictionary, "r")
# Variable to hold the max no of threads to spawn
threadcount = 0
# List of threads
threads = []


# Get the max no of threds to spawn from user
def getThreadCount():
    while True:
        try:
            count = int(raw_input("Max thread count?"))
            return count  # get out of loop if successful
        except ValueError:
            print "Please enter valid integer!!"


def setThreadCount():
    global threadcount
    threadcount = getThreadCount()
    # limit threads to maximum of the credentials count
    if (threadcount > len(credslist)):
        threadcount = len(credslist)
    if threadcount == 0:
        threadcount = 1


def startThreads():
    print "Threads : " + str(threadcount)
    # t1 = datetime.datetime.now()
    for i in range(threadcount):
        t = threading.Thread(target=changePassword, args=(successlog, failurelog, credsq))
        threads.append(t)
        # t.setDaemon(True)
        t.start()
    for t in threads:
        t.join()


def initialise():
    fr = csv.reader(f)
    global passlist
    for password in list(fr):
        passlist.append(password[1])
    passlist = passlist[1:]
    # print passlist


def checkFiles():
    proceed = True
    if (os.path.exists(credentials)) and (os.path.isfile(credentials)):
        print "Credentials File: " + credentials + " OK!"
    else:
        print "Please provide {0} file with initial credentials in the following format:".format(credentials)
        print "IP,Port,Username,Password"
        print "Above header should be the first line in the {0} file.".format(credentials)
        proceed = False
    if (os.path.exists(dictionary)) and (os.path.isfile(dictionary)):
        print "Dictionary File: " + dictionary + " OK!"
    else:
        print "Please provide {0} file with initial credentials in the following format:".format(dictionary)
        print "Index,Password"
        print "Above header should be the first line in the {0} file.".format(dictionary)
        proceed = False
    if proceed == False:
        print "Some file(s) missing!"
        print "Exiting!"
        sys.exit(1)
    else:
        print "All files OK, Good to go!"


def checkLogFiles():
    try:
        if (os.path.exists(successlogfile)) and (os.path.isfile(successlogfile)):
            print "Success Log File : " + successlogfile + " already exists! Renaming for backup!"
            os.rename(successlogfile, successlogfile.split(".")[0] + time.strftime("%d_%b_%Y_%I_%M_%S_%p") + '.' +
                      successlogfile.split(".")[1])
        print "Creating new Success Log File : " + successlogfile
        slog = open(successlogfile, "w")
        # slog.write("IP,Port,Username,Password,Log,Timestamp")
        # slog.close()
        if (os.path.exists(failurelogfile)) and (os.path.isfile(failurelogfile)):
            print "Failure Log File : " + failurelogfile + " already exists! Renaming for backup!"
            os.rename(failurelogfile, failurelogfile.split(".")[0] + time.strftime("%d_%b_%Y_%I_%M_%S_%p") + '.' +
                      failurelogfile.split(".")[1])
        print "Creating new Failure Log File : " + failurelogfile
        flog = open(failurelogfile, "w")
        # flog.write("IP,Port,Username,Password,Log,Timestamp")
        # flog.close()
        return slog, flog
    except IOError as i:
        print i
        response = raw_input("Retry? y or n : ")
        if response == 'y':
            checkLogFiles()
        else:
            print "Exiting!"
            sys.exit(1)
    except WindowsError as w:
        print w
        response = raw_input("Retry? y or n : ")
        if response == 'y':
            checkLogFiles()
        else:
            print "Exiting!"
            sys.exit(1)


def getFullstamp():
    return time.strftime("%d_%b_%Y_%H_%M_%S")


# dictionaryfile -> list for the dictionary file in read only mode
def passwordToIndex(password, dictionary_list):
    return dictionary_list.index(password)


def indexToPassword(index, dictionary_list):
    return dictionary_list[index]


def nextPassword(password, dictionary_list):
    # print str(passwordToIndex(password,dictionary_list)) + " " + str((passwordToIndex(password,dictionary_list)+1)
    # %(len(dictionary_list)))
    if (firstrun and (skip > 0)):
        return indexToPassword(skip - 1, dictionary_list)
    else:
        return indexToPassword(((passwordToIndex(password, dictionary_list) + 1) % len(dictionary_list)), passlist)


def getResponse():
    print "Is this your first run?"
    response = raw_input("y or n :")
    if response == 'y':
        global firstrun
        firstrun = True
        print "Would you like to skip all the existing passwords for new passwords?"
        response = raw_input("y or n :")
        if response == 'y':
            global skip
            while True:
                try:
                    skip = int(raw_input("Enter the index of the password you would like to start from:"))
                    if skip < len(passlist):
                        break
                    else:
                        print "Please enter valid integer!!"
                except ValueError:
                    print "Please enter valid integer!!"

    else:
        firstrun = False
        '''
    try:

        print "Starting"
        print "#" * 60

        # Actual code that changes the password which can be put into thread
        for ip, port, username, passwd in credslist:
            try:
                newPassword = nextPassword(passwd, passlist)
                shell = spur.SshShell(hostname=ip, port=int(port), username=username, password=passwd,
                                      missing_host_key=spur.ssh.MissingHostKey.accept)
                with shell:
                    # result = shell.run(["chage", "-l", "yogi"])
                    command = "echo -e \"{0}\\n{1}\\n{1}\" | passwd".format(passwd, newPassword)
                    result = shell.run(["sh", "-c", command])
                    validity = shell.run(["chage", "-l", username])
                    # print result.output
                validity = validity.output.split("\n")
                # error = validity.stderr_output
                # print op
                # print "Last Password Change" + op[0].split(":")[1]
                # print "Password Expires" + op[1].split(":")[1]
                newstr1 = validity[0].split(":")[1]
                newstr2 = validity[1].split(":")[1]
                swriter.writerow((ip, username, newPassword, newstr1, newstr2, result.output, getFullstamp()))
                print ip + " " + username + " Done"
            except spur.ssh.ConnectionError as e:
                # print "caught exception"
                # print e
                # print type(e)
                fwriter.writerow((ip, username, passwd, e, getFullstamp()));
                print ip + " " + username + " Error"
            except spur.results.RunProcessError as r:
                # print "Something wrong happened!"
                # print r
                # print type(r)
                fwriter.writerow((ip, username, passwd, r, getFullstamp()));
                print ip + " " + username + " Error"
            except spur.errors.NoSuchCommandError as p:
                fwriter.writerow((ip, username, passwd, p, getFullstamp()));
                print ip + " " + username + " Error"
            except ValueError as v:
                fwriter.writerow(
                    (ip, username, passwd, str(v) + "\nPassword not found in dictionary!", getFullstamp()));
                print ip + " " + username + " Error"
            finally:
                pass
                # End of the code
                # print "exiting"
        success.close()
        failure.close()
        print "Completed"
        print "#" * 60
    except IOError as err:
        print err
        print "Exiting!"
        sys.exit(1)
'''


# main code to change the password
def changePassword(slog, flog, qcreds):
    try:
        while True:
            # Get info from the queue, if no info available then Queue.Empty is raised which terminates this
            # thread's execution
            ip, port, username, password = qcreds.get(False)
            try:
                # Get next password from the dictionary with respect to current password
                newPassword = nextPassword(password, passlist)
                shell = spur.SshShell(hostname=ip, port=int(port), username=username, password=password,
                                      missing_host_key=spur.ssh.MissingHostKey.accept)
                with shell:
                    # The actual command that changes the password on linux host
                    command = "echo -e \"{0}\\n{1}\\n{1}\" | passwd".format(password, newPassword)
                    # To pipeline the output shell is necessary
                    result = shell.run(["sh", "-c", command])
                    # Get the password ageing info from the host
                    validity = shell.run(["chage", "-l", username])
                    # result=validity
                # Get lines from the output
                validity = validity.output.split("\n")
                # Get Last Password Change
                newstr1 = validity[0].split(":")[1]
                # Get Password Expiry
                newstr2 = validity[1].split(":")[1]
                # Write command executed succesfully to successlog file
                # Use lock so that only one thread writes to file and stdout
                with lock:
                    slog.writerow(
                        (ip, str(port), username, newPassword, newstr1, newstr2, result.output, getFullstamp()))
                    print ip + " " + username + " Done"
            except spur.ssh.ConnectionError as e:
                # If connection to the target was not completed write to failure log file
                with lock:
                    flog.writerow((ip, str(port), username, password, e, getFullstamp()));
                    print ip + " " + username + " Error"
            except spur.results.RunProcessError as r:
                # If the command reports error
                with lock:
                    flog.writerow((ip, str(port), username, password, r, getFullstamp()));
                    print ip + " " + username + " Error"
            except spur.errors.NoSuchCommandError as p:
                with lock:
                    flog.writerow((ip, str(port), username, password, p, getFullstamp()));
                    print ip + " " + username + " Error"
            except ValueError as v:
                # If current password not found in dictionary file the log error
                with lock:
                    flog.writerow(
                        (ip, str(port), username, password, str(v) + "\nPassword not found in dictionary!",
                         getFullstamp()));
                    print ip + " " + username + " Error"
    except Queue.Empty:
        # Exit this thread
        pass


def closeFiles():
    # Close all open files if any
    if not success.closed:
        success.close()
    if not failure.closed:
        failure.close()
    if not f.closed:
        f.close()
    if not credsfile.closed:
        credsfile.close()


try:
    checkFiles()
    success, failure = checkLogFiles()
    successlog = csv.writer(success, lineterminator="\n")
    failurelog = csv.writer(failure, lineterminator="\n")
    successlog.writerow(
        ("IP", "Port", "Username", "Password", "Last Password Change", "Password Expires", "Log", "Fullstamp"))
    failurelog.writerow(("IP", "Port", "Username", "Password", "Log", "Fullstamp"))
    initialise()
    getResponse()
    # print "skip = " + str(skip) + " Firstrun = " + str(firstrun)
    setThreadCount()
    print "Starting..."
    print "#" * 60
    t1 = datetime.datetime.now()
    startThreads()
    t2 = datetime.datetime.now()
    print "Completed!"
    print "#" * 60
    print "Finished in : " + str(t2 - t1)
except KeyboardInterrupt:
    print "You pressed Ctrl+C ... Exiting!"
    closeFiles()
    sys.exit()
finally:
    # Close all open files if any
    closeFiles()
